package com.twp.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.twp.dao.TwpDao;
import com.twp.domain.AppSettings;
import com.twp.domain.EventDomain;
import com.twp.domain.UserDomain;
import com.twp.domain.UserTaskDomain;
import com.twp.message.request.EditUserTaskForm;
import com.twp.message.request.NewAdminTaskForm;
import com.twp.message.request.NewUserInfoForm;
import com.twp.message.request.UsernameForm;
import com.twp.message.response.ResponseMessage;
import com.twp.model.Category;
import com.twp.model.User;
import com.twp.repository.CategoryRepository;
import com.twp.repository.UserRepository;
import com.twp.security.services.UserService;

/*

 * Copyright (C) 2019 Prime Inc - All Rights Reserved
* Unauthorized use of this file and its contents, via any medium is strictly prohibited.
* Authored by the Missouri State University Computer Science Department
* Fall 2019 CSC 450 - Group 2
* 
 */

// This class contains every endpoint used by the admin users of the application
// Most endpoints catch errors and return a 200 message with a short explanation of the error
// 500 error is not used to avoid the end user viewing the exception directly
// Instead a customized warning can be displayed to the user
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class AdminController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserRepository userRepository;

	@Autowired
	TwpDao tDao;
	
	@Autowired
	CategoryRepository categoryRepository;
	
	//End point for admins to create new health tasks
	@PostMapping(value = "/createNewTask")
	public ResponseEntity<?> enterNewTask(@Valid @RequestBody NewAdminTaskForm newAdminTaskRequest) {
		try {
			tDao.createNewTask(newAdminTaskRequest);
			return new ResponseEntity<>(new ResponseMessage(" Task Created successfully!"), HttpStatus.OK);
		}catch(Exception e) {
			System.out.println("Error: " + e);
			return new ResponseEntity<>(new ResponseMessage(" Something went wrong!"), HttpStatus.NOT_FOUND);
		}	
	}
	
	// End point receives data to be changed about an existing task
	//Allows admins to change values such as the number of points a task is worth
	@PostMapping(value = "/editTask")
	public ResponseEntity<?> editTask(@Valid @RequestBody NewAdminTaskForm newAdminTaskRequest) {
		try {
			tDao.editTask(newAdminTaskRequest);
			return new ResponseEntity<>(new ResponseMessage(" Task Edited successfully!"), HttpStatus.OK);
		}catch(Exception e) {
			System.out.println("Error: " + e);
			return new ResponseEntity<>(new ResponseMessage(" Something went wrong!"), HttpStatus.NOT_FOUND);
		}	
	}
	
	// Returns all tasks with a status of 'Pending'
	@GetMapping(value = "/getPendingUserTasks", produces = "application/json")
	public @ResponseBody List<UserTaskDomain> getPendingTasks() {
		return tDao.getPendingTasks();
	}
	
	// Receives data to be changed about an existing usertask
	// Allows admins to approve or deny submitted tasks
	@PostMapping(value = "/editSubmittedTask")
	public ResponseEntity<?> approveUserTask(@Valid @RequestBody EditUserTaskForm editUserTaskForm) {
		try {
			UserDomain user = tDao.getUserInfo(editUserTaskForm.getUserId());
			if(editUserTaskForm.getStatus().equals("Approved")) {
				tDao.updatePointsApproval(user, editUserTaskForm);	 
			} 		
			tDao.updateUserTask(editUserTaskForm, editUserTaskForm.getStatus());
			
			return new ResponseEntity<>(new ResponseMessage("Task set to status " + editUserTaskForm.getStatus()  + " successfully"), HttpStatus.OK);
		} catch(Exception e) {
			System.out.println("Error: " + e);
			return new ResponseEntity<>(new ResponseMessage("Something went wrong!"), HttpStatus.OK);
		}
		
	}
	
	// Returns all users that have not been assigned to a category and are currently in the Default category
	@GetMapping("/getUnclassifiedUsers")
	public ResponseEntity<?> getUnclassifiedUsers(){
		return new ResponseEntity<>(userRepository.findUnclassifiedUsers(),HttpStatus.OK);
	}
	
	// Returns all existing user categories i.e. Diabetes, Weight Loss, Heart Health
	@GetMapping(value="/getAllCategories")
	public ResponseEntity<?> findAllCategories(){
		return new ResponseEntity<>(categoryRepository.findAll(),HttpStatus.OK);
	}
	
	// Creates a new health category in which users can be placed
	@PostMapping(value="/createCategory")
	public ResponseEntity<?> createCategory(@Valid @RequestBody Category category) {
		try {
			Category newcategory = new Category(category.getName(),category.getQuarterly_goal(),category.getDescription());
			categoryRepository.save(newcategory);
			return new ResponseEntity<>(new ResponseMessage("Category created successfully!"), HttpStatus.OK);
		} catch(Exception e) {
			System.out.println("Error: " + e);
			return new ResponseEntity<>(new ResponseMessage("Something went wrong!"), HttpStatus.OK);
		}	
	}
	
	// Receives data to change an existing category
	@PostMapping(value="/editCategory")
	public ResponseEntity<?> editCategory(@Valid @RequestBody Category category) {
		try {
			categoryRepository.save(category);
			return new ResponseEntity<>(new ResponseMessage("Category edited successfully!"), HttpStatus.OK);
		} catch(Exception e) {
			System.out.println("Error: " + e);
			return new ResponseEntity<>(new ResponseMessage("Something went wrong!"), HttpStatus.OK);
		}	
	}
	
	// Returns all existing users
	@GetMapping(value = "/getAllUsers")
	public List<UserDomain> getAllUsers(){
		return tDao.getAllUsers();
	}
	
	// Edit an existing user, a point goal is set based on the category the edited user is assigned to
	@PostMapping(value = "/editUser")
	public ResponseEntity<?> editUser(@Valid @RequestBody User user){
		Category cat = categoryRepository.findById(user.getCategory().getCategory_id()).get();
		user.setQuarter_goal(cat.getQuarterly_goal());
		userRepository.save(user);
		return new ResponseEntity<>(new ResponseMessage("User saved successfully!"), HttpStatus.OK);
	}
	
	
	// Resets all users weekly point totals to 0
	@GetMapping(value = "/clearUsersWeekly")
	public ResponseEntity<?> clearWeekly() {
		List<User> userList = userService.findAllUsers();
		for (User user : userList) {
			user.setWeek_total((long) 0);
			userRepository.save(user);
		}
		return new ResponseEntity<>(new ResponseMessage("Users weekly totals reset"), HttpStatus.OK);
	}
	
	// Resets all users quarterly point totals to 0
	@GetMapping(value = "/clearUsersQuarter")
	public ResponseEntity<?> clearQuarter() {
		List<User> userList = userService.findAllUsers();
		for (User user : userList) {
			user.setQuarter_total((long) 0);
			userRepository.save(user);
		}
		return new ResponseEntity<>(new ResponseMessage("Users quarterly totals reset"), HttpStatus.OK);
	}
	
	// Creates a new user
	@PostMapping(value = "/addNewImportedUser")
	public ResponseEntity<?> addNewImportedUser(@Valid @RequestBody NewUserInfoForm userInfo) {
		try {
			tDao.addNewImportedUser(userInfo);
			return new ResponseEntity<>(new ResponseMessage("New Patient added successfully"), HttpStatus.OK);
		}catch(Exception e) {
			System.out.println("Error: " + e);
			return new ResponseEntity<>(new ResponseMessage("Something went wrong!"), HttpStatus.OK);
		}		
	}
	
	// Allows the admin to change the quarter end date displayed to the users in the frontend
	// Quarter end date does not affect functionality as point totals are reset manuall by the user
	// This value is only displayed to the user
	@PostMapping(value = "/adjustQuarterDate")
	public ResponseEntity<?> adjustQuarterDate(@Valid @RequestBody String date){
		tDao.adjustQuarterDate(date);
		return new ResponseEntity<>(new ResponseMessage("Date adjusted successfully"), HttpStatus.OK);
	}
	
	// Returns the current app settings such as Admin name and contact information
	// These values are displayed to patient users
	@PostMapping(value = "/getAppSettings")
	public List<AppSettings> getAppSetting(){
		return tDao.getAppSettings();
	}
	
	// Allows the admin(s) to change the values displayed to users
	@PostMapping(value = "/editAppSettings")
	public ResponseEntity<?> editAppSetting(@Valid @RequestBody AppSettings appSettings){
		try {
			tDao.editAppSettings(appSettings);
			return new ResponseEntity<>(new ResponseMessage("App Setting adjusted successfully"), HttpStatus.OK);
		} catch(Exception e) {
			System.out.println("Error: " + e);
			return new ResponseEntity<>(new ResponseMessage("Something went wrong!"), HttpStatus.OK);
		}		
	}
	
	// Creates a new calendar event
	@PostMapping(value = "/createNewEvent")
	public ResponseEntity<?> createNewEvent(@Valid @RequestBody EventDomain event){
		try {
			tDao.createNewEvent(event);
			return new ResponseEntity<>(new ResponseMessage("Event Created Successfully"), HttpStatus.OK);
		} catch(Exception e) {
			System.out.println("Error: " + e);
			return new ResponseEntity<>(new ResponseMessage("Something went wrong!"), HttpStatus.OK);
		}		
	}
	
	// Edit an existing calendar event
	@PostMapping(value = "/editEvent")
	public ResponseEntity<?> editEvent(@Valid @RequestBody EventDomain event){
		try {
			tDao.editEvent(event);
			return new ResponseEntity<>(new ResponseMessage("Event Edited Successfully"), HttpStatus.OK);
		} catch(Exception e) {
			System.out.println("Error: " + e);
			return new ResponseEntity<>(new ResponseMessage("Something went wrong!"), HttpStatus.OK);
		}		
	}
	
	// Returns all the existing, submitted tasks of a specific user
	// Allows the admin(s) to review previous task entries of a user
	@PostMapping(value = "/getUserHistory")
	public List<UserTaskDomain> getUserTaskHistory(@Valid @RequestBody UsernameForm username){
		 return tDao.getAllUserHistoryTasks(username.getUsername());
	}
}
