/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */

package com.twp.controller;

// Class:	AuthRestApis.java
// Purpose:	Handles all signing in and up of new users
//			Contains methods and endpoints for all current and new users
//			to sign in and sign up

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.twp.dao.TwpDao;
import com.twp.domain.UserInfoDomain;
import com.twp.message.request.LoginForm;
import com.twp.message.request.NewQuestionForm;
import com.twp.message.request.ResetForm;
import com.twp.message.request.SignUpForm;
import com.twp.message.request.UsernameForm;
import com.twp.message.response.JwtResponse;
import com.twp.message.response.QuestionsResponse;
import com.twp.message.response.ResponseMessage;
import com.twp.model.Category;
import com.twp.model.Question;
import com.twp.model.Role;
import com.twp.model.RoleName;
import com.twp.model.User;
import com.twp.repository.CategoryRepository;
import com.twp.repository.QuestionRepository;
import com.twp.repository.RoleRepository;
import com.twp.repository.UserRepository;
import com.twp.security.jwt.JwtProvider;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthRestApis {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;
	
	@Autowired
	TwpDao tDao;
	
	@Autowired
	CategoryRepository categoryRepository;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateJwtToken(authentication);
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();

		return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
		
		UserInfoDomain userInfo = tDao.checkIfEmployeeCodeExists(signUpRequest.getUsername());
		
		if(userInfo.getEmployeeCode() == null) {
			return new ResponseEntity<>(new ResponseMessage("Error: Employee code doesn't exist"),
					HttpStatus.OK);
		}
		
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ResponseMessage("Error: Username is already taken!"),
					HttpStatus.OK);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<>(new ResponseMessage("Error: Email is already in use!"),
					HttpStatus.OK);
		}
		
		// Creating user's account
		User user = new User(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()), signUpRequest.getWeek_goal(), signUpRequest.getQuarter_goal(),
				signUpRequest.getWeek_total(),signUpRequest.getQuarter_total(),signUpRequest.getQuestion1(),signUpRequest.getQuestion2(),
				signUpRequest.getQuestion3(), encoder.encode(signUpRequest.getAnswer1().toLowerCase()),encoder.encode(signUpRequest.getAnswer2().toLowerCase()),encoder.encode(signUpRequest.getAnswer3().toLowerCase()),
				userInfo.getEmployeePayrollCode()); 

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		// Assigning a role to the new user
		strRoles.forEach(role -> {
			switch (role) {
			case "admin":
				Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Error: Cause: Admin Role not found."));
				roles.add(adminRole);

				break;
			case "moderator":
				Role moderatorRole = roleRepository.findByName(RoleName.ROLE_MODERATOR)
						.orElseThrow(() -> new RuntimeException("Error: Cause: Moderator Role not found."));
				roles.add(moderatorRole);

				break;
			default:
				Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
						.orElseThrow(() -> new RuntimeException("Error: Cause: User Role not found."));
				roles.add(userRole);
			}
		});
		
		// Assigning a new user to the category Default
		Category category = categoryRepository.findByName("Default");

		user.setRoles(roles);
		user.setCategory(category);
		user.setQuarter_goal(category.getQuarterly_goal());
		userRepository.save(user);

		return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
	}
	
	@Autowired
	QuestionRepository questionRepository;
	
	@PostMapping("/newquestion")
	public ResponseEntity<?> createQuestion(@Valid @RequestBody NewQuestionForm question) {	
		Question newquestion = new Question(question.getQuestion());	
		questionRepository.save(newquestion);	
		return new ResponseEntity<>(new ResponseMessage("New question created!"), HttpStatus.OK);	
	}
	
	@GetMapping("/getallquestions")
	public ResponseEntity<?> getAllUsers() {
		return new ResponseEntity<>(questionRepository.findAll(), HttpStatus.OK);
	}
	
	@PostMapping("/getquestions")
	public ResponseEntity<?> createQuestion(@Valid @RequestBody UsernameForm username) {
		User user = userRepository.findByUsername(username.getUsername()).get();
		QuestionsResponse qr = new QuestionsResponse(user.getQuestion1(),user.getQuestion2(),user.getQuestion3());
		return new ResponseEntity<>(new QuestionsResponse(user.getQuestion1(),user.getQuestion2(),user.getQuestion3()), HttpStatus.OK);
		
	}
	
	@PostMapping("/resetpassword")
	public ResponseEntity<?> resetPassword(@Valid @RequestBody ResetForm resetinfo) {	
		User user = userRepository.findByUsername(resetinfo.getUsername()).get();
		int correct = 0;
		if(encoder.matches(resetinfo.getAnswer1().toLowerCase(),user.getAnswer1())) {
			correct += 1;
		}
		if(encoder.matches(resetinfo.getAnswer2().toLowerCase(), user.getAnswer2())) {
			correct += 1;
		}
		if(encoder.matches(resetinfo.getAnswer3().toLowerCase(), user.getAnswer3())) {
			correct += 1;
		}
		if(correct < 2) {
			return new ResponseEntity<>(new ResponseMessage("Password reset failed!"), HttpStatus.OK);
		}else {
			user.setPassword(encoder.encode(resetinfo.getPassword()));
			userRepository.save(user);
			return new ResponseEntity<>(new ResponseMessage("Password reset succeeded!"), HttpStatus.OK);

		}
	}
}