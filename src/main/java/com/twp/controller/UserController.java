/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */

package com.twp.controller;

// Class:	UserController.java
// Purpose:	Directs the flow and functionality for the user page
//			Contains endpoints and methods used by the User page

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.twp.dao.TwpDao;
import com.twp.domain.EventDomain;
import com.twp.domain.TaskDomain;
import com.twp.domain.UserDomain;
import com.twp.domain.UserTaskDomain;
import com.twp.message.request.LoginForm;
import com.twp.message.request.NewUserTaskForm;
import com.twp.message.request.UsernameForm;
import com.twp.message.response.ResponseMessage;
import com.twp.model.User;
import com.twp.repository.UserRepository;
import com.twp.security.services.UserService;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {
	
	private static final LoginForm usernameRequest = null;

	User user;

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserService userService;

	@Autowired
	TwpDao tDao;	
	
	@PostMapping(value = "/getUserInfo", produces = "application/json")
	public @ResponseBody UserDomain getUserInfo(@Valid @RequestBody UsernameForm username) throws Exception {
		return tDao.getUserInfo(username.getUsername());
	}

	@PostMapping("/getuser")
	public ResponseEntity<?> getUserWithUsername(@Valid @RequestBody UsernameForm usernameRequest) {
		return new ResponseEntity<>(userService.getUserByUsername(usernameRequest.getUsername()), HttpStatus.OK);
	}

	@GetMapping("/getallusers")
	public ResponseEntity<?> getAllUsers() {
		return new ResponseEntity<>(userService.findAllUsers(), HttpStatus.OK);
	}	
	
	@GetMapping("/getcompliantusers")
	public ResponseEntity<?> getCompliantUsers(){
		return new ResponseEntity<>(userRepository.findCompliantUsers(),HttpStatus.OK);
	}
	
	@GetMapping("/getNonCompliantUsers")
	public ResponseEntity<?> getNonCompliantUsers(){
		return new ResponseEntity<>(userRepository.findNonCompliantUsers(),HttpStatus.OK);
	}

	@GetMapping(value = "/getEvents", produces = "application/json")
	public @ResponseBody List<EventDomain> getAllEvents(Model model) throws Exception {
		return tDao.getAllEvents();
	}

	@GetMapping(value = "/getTasks", produces = "application/json")
	public @ResponseBody List<TaskDomain> getAllTasks(Model model) throws Exception {
		return tDao.getAllTasks();
	}

	@PostMapping(value = "/submitTasks")
	public ResponseEntity<?> submitTask(@Valid @RequestBody NewUserTaskForm newUserTaskRequest) {
	
		User user = userRepository.findByUsername(newUserTaskRequest.getUsername()).get();
		
		List<UserTaskDomain> submittedTasks = tDao.getAllUserTasks(newUserTaskRequest.getUsername());
		List<UserTaskDomain> thisSubmittedTask = new ArrayList<UserTaskDomain>();
		for(int i=0;i<submittedTasks.size();i++) {
			if(newUserTaskRequest.getTaskId()==submittedTasks.get(i).getTaskId()) {
				thisSubmittedTask.add(submittedTasks.get(i));
			}
		}
		TaskDomain thisTask = tDao.getTask(newUserTaskRequest.getTaskId());
		int taskMaxSubmissions = thisTask.getTaskMax() / thisTask.getTaskPoints();
		LocalDateTime date = LocalDateTime.now();
		TemporalField woy = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();
		int day = date.getDayOfYear();
		int week = date.get(woy);
		int month = date.getMonthValue();
		int year = date.getYear();
		int prevSubmitted = 0;
		
		// The following below ensures a user doesn't submit the task more than the max frequency
		if(thisTask.getTaskFreq().contentEquals("Daily")){
			for(int i=0;i<thisSubmittedTask.size();i++) {
				Instant instant = Instant.parse(thisSubmittedTask.get(i).getCompletionDate());
				LocalDateTime completionDate = LocalDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));
				int completionDay = completionDate.getDayOfYear();
				int completionYear = completionDate.getYear();
				if(day == completionDay && year == completionYear) {
					prevSubmitted += 1;
				}
			}
		} else if(thisTask.getTaskFreq().contentEquals("Weekly")) {
			for(int i=0;i<thisSubmittedTask.size();i++) {
				Instant instant = Instant.parse(thisSubmittedTask.get(i).getCompletionDate());
				LocalDateTime completionDate = LocalDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));
				int completionWeek = completionDate.get(woy);
				int completionYear = completionDate.getYear();
				if(week == completionWeek && year == completionYear) {
					prevSubmitted += 1;
				}
			}
		} else if (thisTask.getTaskFreq().contentEquals("Monthly")) {
			for(int i=0;i<thisSubmittedTask.size();i++) {
				Instant instant = Instant.parse(thisSubmittedTask.get(i).getCompletionDate());
				LocalDateTime completionDate = LocalDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));
				int completionMonth = completionDate.getMonthValue();
				int completionYear = completionDate.getYear();
				if(month == completionMonth && year == completionYear) {
					prevSubmitted += 1;
				}
			}
		} else if (thisTask.getTaskFreq().contentEquals("Quarterly")) {
			for(int i=0;i<thisSubmittedTask.size();i++) {
				Instant instant = Instant.parse(thisSubmittedTask.get(i).getCompletionDate());
				LocalDateTime completionDate = LocalDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));
				int completionWeek = completionDate.get(woy);
				int completionYear = completionDate.getYear();
				if(year == completionYear) {
					if(week > 0 && week < 14) {
						if(completionWeek > 0 && completionWeek < 14) {
							prevSubmitted += 1;
						}
					} else if(week > 13 && week < 27) {
						if(completionWeek > 13 && completionWeek < 27) {
							prevSubmitted += 1;
						}
					} else if(week > 26 && week < 40) {
						if(completionWeek > 26 && completionWeek < 40) {
							prevSubmitted += 1;
						}
					} else if(week > 39) {
						if(completionWeek > 39) {
							prevSubmitted += 1;
						}
					}
				}
			}
		} else if (thisTask.getTaskFreq().contentEquals("Semi-Annual")) {
			for(int i=0;i<thisSubmittedTask.size();i++) {
				Instant instant = Instant.parse(thisSubmittedTask.get(i).getCompletionDate());
				LocalDateTime completionDate = LocalDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));
				int completionWeek = completionDate.get(woy);
				int completionYear = completionDate.getYear();
				if(year == completionYear) {
					if(week > 0 && week < 27) {
						if(completionWeek > 0 && completionWeek < 27) {
							prevSubmitted += 1;
						}
					} else if(week > 26) {
						if(completionWeek > 26) {
							prevSubmitted += 1;
						}
					}
				}
			}
		} else if (thisTask.getTaskFreq().contentEquals("Yearly")) {
			for(int i=0;i<thisSubmittedTask.size();i++) {
				Instant instant = Instant.parse(thisSubmittedTask.get(i).getCompletionDate());
				LocalDateTime completionDate = LocalDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));
				int completionYear = completionDate.getYear();
				if(year == completionYear) {
					prevSubmitted += 1;
				}
			}
		}
		if(prevSubmitted >= taskMaxSubmissions) {
			return new ResponseEntity<>(new ResponseMessage("Error: MAX_LIMIT_REACHED"), HttpStatus.OK);
		}
		
		boolean points;
		if(!"Pending".equals(newUserTaskRequest.getStatus())) {
			points = tDao.updatePoints(user, newUserTaskRequest);
		} else {
			points = false;
		}
		
		if(points) {
			try {
				tDao.submitUserTask(newUserTaskRequest);
				return new ResponseEntity<>(new ResponseMessage("User Task Created successfully!"), HttpStatus.OK);
			} catch(Exception e) {
				return new ResponseEntity<>(new ResponseMessage("Error: Something went wrong!"), HttpStatus.OK);
			}
		}else {
			try {
				tDao.submitUserTask(newUserTaskRequest);
				return new ResponseEntity<>(new ResponseMessage("User Task Created successfully!"), HttpStatus.OK);
			} catch(Exception e) {
				return new ResponseEntity<>(new ResponseMessage("Error: Something went wrong!"), HttpStatus.OK);
			}
		}
	}
	
	 @PostMapping(value = "/getUserTasks", produces = "application/json") 
	 public @ResponseBody List<UserTaskDomain> getUserTasks(@Valid @RequestBody UsernameForm username) throws Exception {
		 return tDao.getAllUserTasks(username.getUsername());
	 }
}
