package com.twp.dao;

import java.util.List;
import javax.validation.Valid;
import org.springframework.stereotype.Service;
import com.twp.domain.AppSettings;
import com.twp.domain.EventDomain;
import com.twp.domain.TaskDomain;
import com.twp.domain.UserDomain;
import com.twp.domain.UserInfoDomain;
import com.twp.domain.UserTaskDomain;
import com.twp.message.request.EditUserTaskForm;
import com.twp.message.request.NewAdminTaskForm;
import com.twp.message.request.NewUserInfoForm;
import com.twp.message.request.NewUserTaskForm;
import com.twp.model.User;

/*

* Copyright (C) 2019 Prime Inc - All Rights Reserved
* Unauthorized use of this file and its contents, via any medium is strictly prohibited.
* Authored by the Missouri State University Computer Science Department
* Fall 2019 CSC 450 - Group 2 
 
 */

// Interface class for TwpDaoImpl detailed documentation can be found there
@Service
public interface TwpDao {

	public List<EventDomain> getAllEvents();
	
	public List<TaskDomain> getAllTasks();
	
	public void submitUserTask(NewUserTaskForm newUserTaskRequest);
	
	public List<UserTaskDomain> getAllUserTasks(String username);
	
	public UserDomain getUserInfo(String username);
	
	public boolean updatePoints(User user, NewUserTaskForm newUserTaskRequest);
	
	public void createNewTask(@Valid NewAdminTaskForm newAdminTaskRequest);
	
	public void editTask(@Valid NewAdminTaskForm newAdminTaskRequest);
	
	public List<UserTaskDomain> getPendingTasks();
	
	public UserInfoDomain checkIfEmployeeCodeExists(String username);
	
	public List<UserDomain> getUnclassifiedUsers();
	
	public List<UserDomain> getAllUsers();
	
	public void updateUserTask(@Valid EditUserTaskForm editUserTaskForm, String update);
	
	boolean updatePointsApproval(UserDomain user, EditUserTaskForm editUserTaskRequest);
	
	public void addNewImportedUser(@Valid NewUserInfoForm userInfo);
	
	public TaskDomain getTask(int task_id);
	
	public void adjustQuarterDate(String date);
	
	public List<AppSettings> getAppSettings();
	
	public void editAppSettings(@Valid AppSettings appSettings);
	
	public void createNewEvent(@Valid EventDomain event);
	
	public void editEvent(@Valid EventDomain event);
	
	public List<UserTaskDomain> getAllUserHistoryTasks(String username);
	
}
