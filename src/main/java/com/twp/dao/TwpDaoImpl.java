/*

* Copyright (C) 2019 Prime Inc - All Rights Reserved
* Unauthorized use of this file and its contents, via any medium is strictly prohibited.
* Authored by the Missouri State University Computer Science Department
* Fall 2019 CSC 450 - Group 2 
 
 */

package com.twp.dao;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import com.twp.domain.AppSettings;
import com.twp.domain.AppSettingsRowMapper;
import com.twp.domain.EventDomain;
import com.twp.domain.EventDomainRowMapper;
import com.twp.domain.PendingUserTaskRowMapper;
import com.twp.domain.TaskDomain;
import com.twp.domain.TaskDomainRowMapper;
import com.twp.domain.UserDomain;
import com.twp.domain.UserDomainRowMapper;
import com.twp.domain.UserInfoDomain;
import com.twp.domain.UserInfoDomainRowMapper;
import com.twp.domain.UserTaskDomain;
import com.twp.domain.UserTaskDomainRowMapper;
import com.twp.message.request.EditUserTaskForm;
import com.twp.message.request.NewAdminTaskForm;
import com.twp.message.request.NewUserInfoForm;
import com.twp.message.request.NewUserTaskForm;
import com.twp.model.User;

// Class:	TwpDaoImpl
//Purpose:	Serves as a connection to the database and handles updates, inserts, and selects 
//			Contains methods and String variables as SQL strings. Used to manipulate the database
//			Makes use of NamedParameterJdbcTemplate and RowMapper classes


@Service
public class TwpDaoImpl implements TwpDao{
	
	// namedParameterJdbcTemplate is used to connect with database and perform database commands
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	final static String getAllEvents = "SELECT * FROM event";
	
	@Override
	public List<EventDomain> getAllEvents() {	
		return namedParameterJdbcTemplate.query(getAllEvents, new EventDomainRowMapper());
	}
	
	final static String getAllTasks = "SELECT * FROM task";

	@Override
	public List<TaskDomain> getAllTasks() {
		return namedParameterJdbcTemplate.query(getAllTasks, new TaskDomainRowMapper());
	}
	
	final static String insertUserTask = "INSERT INTO submitted_tasks VALUES ("
			+ ":task_id, :user_id, :task_points, :timestamp, :photo, :status, :description)";

	@Override
	public void submitUserTask(NewUserTaskForm newUserTaskRequest) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("task_id", newUserTaskRequest.getTaskId());
		parameters.addValue("user_id", newUserTaskRequest.getUsername());
		parameters.addValue("task_points", newUserTaskRequest.getTaskPoints());
		parameters.addValue("timestamp", newUserTaskRequest.getCompletionDate());
		parameters.addValue("photo", newUserTaskRequest.getPhoto());
		parameters.addValue("status", newUserTaskRequest.getStatus());
		parameters.addValue("description", newUserTaskRequest.getDescription());
		namedParameterJdbcTemplate.update(insertUserTask, parameters);
	}
	
	final static String updateUserPoints = "UPDATE users SET quarter_total = :quarter_total, week_total = :week_total WHERE username = :username";
	
	@Override
	public boolean updatePoints(User user, NewUserTaskForm newUserTaskRequest) {
		
		int weekTotal = user.getWeek_total().intValue();
		int points = newUserTaskRequest.getTaskPoints();
		int newWeekTotal = user.getWeek_total().intValue() + newUserTaskRequest.getTaskPoints();
		int quarterTotal = user.getQuarter_total().intValue() + newUserTaskRequest.getTaskPoints();
		
		if (weekTotal >= user.getWeek_goal()) {
			return false;
		} else if((points + weekTotal) >= user.getWeek_goal()){
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("week_total", user.getWeek_goal());
			parameters.addValue("quarter_total", quarterTotal);
			parameters.addValue("username", user.getUsername());
			namedParameterJdbcTemplate.update(updateUserPoints, parameters);
			return true;
		} else {
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("week_total", newWeekTotal);
			parameters.addValue("quarter_total", quarterTotal);
			parameters.addValue("username", user.getUsername());
			namedParameterJdbcTemplate.update(updateUserPoints, parameters);
			return true;
		}	
	}
	
	@Override
	public boolean updatePointsApproval(UserDomain user, EditUserTaskForm editUserTaskRequest) {
		
		int weekTotal = user.getWeekTotal();
		int points = editUserTaskRequest.getTaskPoints();
		int newWeekTotal = user.getWeekTotal() + editUserTaskRequest.getTaskPoints();
		int quarterTotal = user.getQuarterTotal() + editUserTaskRequest.getTaskPoints();
		
		// If the user has already reached their weekly point limit they cannot log additional points
		if (weekTotal >= user.getWeekGoal()) {
			return false;
		} else if((points + weekTotal) >= user.getWeekGoal()){
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("week_total", user.getWeekGoal());
			parameters.addValue("quarter_total", user.getQuarterTotal() + (user.getWeekGoal() - user.getQuarterTotal()));
			parameters.addValue("username", editUserTaskRequest.getUserId());
			namedParameterJdbcTemplate.update(updateUserPoints, parameters);
			return true;
		} else {
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("week_total", newWeekTotal);
			parameters.addValue("quarter_total", quarterTotal);
			parameters.addValue("username", editUserTaskRequest.getUserId());
			namedParameterJdbcTemplate.update(updateUserPoints, parameters);
			return true;
		}
	}
	
	final static String getUserTasks = "SELECT TOP 25 u.user_task_id, u.task_id, u.user_id, t.task_name, u.task_points, u.timestamp, u.status, u.description "
			+ "FROM submitted_tasks u "
			+ "INNER JOIN task t ON u.task_id = t.task_id "
			+ "INNER JOIN users s ON u.user_id = s.username "
			+ "WHERE u.user_id = :username "
			+ "ORDER BY timestamp DESC";

	@Override
	public List<UserTaskDomain> getAllUserTasks(String username) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("username", username.toString());
		return namedParameterJdbcTemplate.query(getUserTasks, parameters, new UserTaskDomainRowMapper());
	}

	final static String getUserInfo = "SELECT * FROM users WHERE username = :username";
	
	@Override
	public UserDomain getUserInfo(String username) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("username", username.toString());
		List<UserDomain> user = namedParameterJdbcTemplate.query(getUserInfo, parameters, new UserDomainRowMapper());
		return user.get(0);
	}
	
	final static String getCompliantUsers = "SELECT * from users u "
			+ "WHERE quarter_total >= quarter_goal";
	
	final static String createNewTask = "INSERT INTO task VALUES("
			+ ":task_name, :task_action, :task_points, :task_max, :task_freq, :photo_required, :verify_required)";

	@Override
	public void createNewTask(@Valid NewAdminTaskForm newAdminTaskRequest) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("task_name", newAdminTaskRequest.getTaskName());
		parameters.addValue("task_action", newAdminTaskRequest.getTaskAction());
		parameters.addValue("task_points", newAdminTaskRequest.getTaskPoints());
		parameters.addValue("task_freq", newAdminTaskRequest.getTaskFreq());
		parameters.addValue("task_max", newAdminTaskRequest.getTaskMax());
		parameters.addValue("photo_required", newAdminTaskRequest.isPhotoRequired());
		parameters.addValue("verify_required", newAdminTaskRequest.isVerificationRequired());
		namedParameterJdbcTemplate.update(createNewTask, parameters);
	}
	
	final static String editTask = "UPDATE task "
			+ "SET task_name = :task_name, task_action = :task_action, task_points = :task_points, "
			+ "task_max = :task_max, task_freq = :task_freq, photo_required = :photo_required, verify_required = :verify_required "
			+ "WHERE task_id = :task_id";

	@Override
	public void editTask(@Valid NewAdminTaskForm newAdminTaskRequest) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("task_id", newAdminTaskRequest.getTaskId());
		parameters.addValue("task_name", newAdminTaskRequest.getTaskName());
		parameters.addValue("task_action", newAdminTaskRequest.getTaskAction());
		parameters.addValue("task_points", newAdminTaskRequest.getTaskPoints());
		parameters.addValue("task_max", newAdminTaskRequest.getTaskMax());
		parameters.addValue("task_freq", newAdminTaskRequest.getTaskFreq());
		if(newAdminTaskRequest.isPhotoRequired() == true) {
			parameters.addValue("photo_required", 1);
		} else {
			parameters.addValue("photo_required", 0);
		}	
		if(newAdminTaskRequest.isVerificationRequired() == true) {
			parameters.addValue("verify_required", 1);
		} else {
			parameters.addValue("verify_required", 0);
		}
		namedParameterJdbcTemplate.update(editTask, parameters);
	}
	
	final static String getPendingTasks = "SELECT * FROM submitted_tasks u "
			+ "INNER JOIN task t ON u.task_id = t.task_id "
			+ "INNER JOIN users s ON u.user_id = s.username "
			+ "WHERE u.status = 'Pending'";
	
	@Override
	public List<UserTaskDomain> getPendingTasks() {
		return namedParameterJdbcTemplate.query(getPendingTasks, new PendingUserTaskRowMapper());
	}

	final static String checkIfEmployeeCodeExists = "SELECT * FROM imported_users WHERE employee_code = :employee_code";
	
	@Override
	public UserInfoDomain checkIfEmployeeCodeExists(String username) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("employee_code", username);
		UserInfoDomain userInfo;
		try {
			userInfo = namedParameterJdbcTemplate.queryForObject(checkIfEmployeeCodeExists, parameters, new UserInfoDomainRowMapper());
			return userInfo;
		} catch(Exception e) {
			return new UserInfoDomain();
		}
	}
	
	final static String getUnclassifiedUsers = "SELECT * FROM users WHERE category_id = 2";

	@Override
	public List<UserDomain> getUnclassifiedUsers() {
		return namedParameterJdbcTemplate.query(getUnclassifiedUsers, new UserDomainRowMapper());
	}

	final static String getAllUsers = "SELECT  FROM users u INNER JOIN user_roles r ON u.id = r.user_id WHERE r.role_id = 3";
	
	@Override
	public List<UserDomain> getAllUsers() {
		return namedParameterJdbcTemplate.query(getAllUsers, new UserDomainRowMapper());
	}
	
	final static String updateUserTask = "UPDATE submitted_tasks SET status = :status WHERE user_task_id = :user_task_id";

	@Override
	public void updateUserTask(@Valid EditUserTaskForm editUserTaskForm, String update) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("status", update);
		parameters.addValue("user_task_id", editUserTaskForm.getUserTaskId());
		namedParameterJdbcTemplate.update(updateUserTask, parameters);
	}
	
	final static String insertIntoUserInfo = "INSERT INTO imported_users VALUES ("
			+ ":employee_name, :employee_code, :payroll_code, :dob)";

	@Override
	public void addNewImportedUser(@Valid NewUserInfoForm userInfo) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("employee_name", userInfo.getName());
		parameters.addValue("employee_code", userInfo.getEmployeeCode());
		parameters.addValue("payroll_code", userInfo.getPayrollCode());
		parameters.addValue("dob", userInfo.getDOB());
		namedParameterJdbcTemplate.update(insertIntoUserInfo, parameters);
	}
	
	final static String getSpecifiedTask = "SELECT * FROM task WHERE task_id = :task_id";
	
	@Override
	public TaskDomain getTask(int task_id) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("task_id", task_id);
		List<TaskDomain> tasks = namedParameterJdbcTemplate.query(getSpecifiedTask, parameters, new TaskDomainRowMapper());
		return tasks.get(0);
	}
	
	final static String adjustDate = "UPDATE appsettings SET value = :date WHERE app_id = 1";

	@Override
	public void adjustQuarterDate(String date) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("date", date);
		namedParameterJdbcTemplate.update(adjustDate, parameters);
	}
	
	final static String getAppSettings = "SELECT * FROM appsettings";

	@Override
	public List<AppSettings> getAppSettings() {
		return namedParameterJdbcTemplate.query(getAppSettings, new AppSettingsRowMapper());
	}
	
	final static String editAppSettings = "UPDATE appsettings SET name = :name, value = :value WHERE app_id = :id";

	@Override
	public void editAppSettings(@Valid AppSettings appSettings) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("name", appSettings.getName());
		parameters.addValue("value", appSettings.getValue());
		parameters.addValue("id", appSettings.getAppId());
		namedParameterJdbcTemplate.update(editAppSettings, parameters);
	}
	
	final static String createNewEvent = "INSERT INTO event VALUES ("
			+ ":title, :description, :date, :start, :end, :link)";

	@Override
	public void createNewEvent(@Valid EventDomain event) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("title", event.getTitle());
		parameters.addValue("description", event.getDescription());
		parameters.addValue("date", event.getDate());
		parameters.addValue("start", event.getStart());
		parameters.addValue("end", event.getEnd());
		parameters.addValue("link", event.getLink());
		namedParameterJdbcTemplate.update(createNewEvent, parameters);
	}
	
	final static String editEvent =  "UPDATE event SET title = :title, description = :description, date = :date, "
			+ "start_time = :start, end_time = :end, link = :link WHERE event_id = :event_id";

	@Override
	public void editEvent(@Valid EventDomain event) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("event_id", event.getEventId());
		parameters.addValue("title", event.getTitle());
		parameters.addValue("description", event.getDescription());
		parameters.addValue("date", event.getDate());
		parameters.addValue("start", event.getStart());
		parameters.addValue("end", event.getEnd());
		parameters.addValue("link", event.getLink());
		namedParameterJdbcTemplate.update(editEvent, parameters);
	}
	
	final static String getUserHistoryTasks = "SELECT u.user_task_id, u.task_id, u.user_id, t.task_name, u.task_points, u.timestamp, u.status, u.description, u.photo "
			+ "FROM submitted_tasks u "
			+ "INNER JOIN task t ON u.task_id = t.task_id "
			+ "INNER JOIN users s ON u.user_id = s.username "
			+ "WHERE u.user_id = :username "
			+ "ORDER BY timestamp";

	@Override
	public List<UserTaskDomain> getAllUserHistoryTasks(String username) {
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("username", username);
		return namedParameterJdbcTemplate.query(getUserHistoryTasks, parameters, new UserTaskDomainRowMapper());
	}
}
