/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */

package com.twp.domain;

// Class:	AppSettingsRowMapper
// Purpose:	Used to map values selected from the table appsettings

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AppSettingsRowMapper implements RowMapper<AppSettings>{

	@Override
	public AppSettings mapRow(ResultSet rs, int rowNum) throws SQLException {
		AppSettings app = new AppSettings();
		
		app.setAppId(rs.getInt("app_id"));
		app.setName(rs.getString("name"));
		app.setValue(rs.getString("value"));
		
		return app;
	}

}
