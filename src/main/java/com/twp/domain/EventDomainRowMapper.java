/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */

package com.twp.domain;

// Class:	EventDomainRowMapper
// Purpose:	Used to map values selected from the table event

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;


public class EventDomainRowMapper implements RowMapper<EventDomain>{
	
	@Override
	public EventDomain mapRow(ResultSet rs, int rowNum) throws SQLException {
		EventDomain event = new EventDomain();
			
		event.setEventId(rs.getInt("event_id"));
		event.setTitle(rs.getString("title"));
		event.setDescription(rs.getString("description"));
		event.setDate(rs.getString("date"));
		event.setStart(rs.getString("start_time"));
		event.setEnd(rs.getString("end_time"));
		event.setLink(rs.getString("link"));
		
		return event;
		
	}
}
