package com.twp.domain;

public class ExportedUserDomain {
	
	private String name;
	private String username;
	private String payroll_code;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPayroll_code() {
		return payroll_code;
	}
	public void setPayroll_code(String payroll_code) {
		this.payroll_code = payroll_code;
	}
	
	
}
