/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */

package com.twp.domain;

// Class:	ExportedUserDomainRowMapper
// Purpose:	Used to map values selected from the table

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ExportedUserDomainRowMapper implements RowMapper<ExportedUserDomain> {
	@Override
	public ExportedUserDomain mapRow(ResultSet rs, int rowNum) throws SQLException {
		ExportedUserDomain exporteduser = new ExportedUserDomain();
		
		exporteduser.setName(rs.getString("name"));
		exporteduser.setUsername(rs.getString("username"));
		exporteduser.setPayroll_code(rs.getString("payroll_code"));

		return exporteduser;
	}
}
