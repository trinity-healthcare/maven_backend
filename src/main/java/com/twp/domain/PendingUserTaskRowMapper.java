/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */

package com.twp.domain;

// Class:	PendingUserTaskRowMapper
// Purpose:	Used to map values selected from the table submitted_tasks and task

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PendingUserTaskRowMapper implements RowMapper<UserTaskDomain>{
	
	@Override
	public UserTaskDomain mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserTaskDomain userTasks = new UserTaskDomain();
		
		userTasks.setUserTaskId(rs.getInt("user_task_id"));
		userTasks.setTaskId(rs.getInt("task_id"));
		userTasks.setUserId(rs.getString("user_id"));
		userTasks.setTaskName(rs.getString("task_name"));
		userTasks.setTaskPoints(rs.getInt("task_points"));
		userTasks.setCompletionDate(rs.getString("timestamp"));
		userTasks.setStatus(rs.getString("status"));
		userTasks.setPhoto(rs.getString("photo"));
		userTasks.setDescription(rs.getString("description"));
		
		return userTasks;
	}
}
