/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */

package com.twp.domain;

// A health task created by an admin and available for a patient to log for points
public class TaskDomain {
	
	private Integer taskId;
	private String taskName;
	private String taskAction;
	private Integer taskPoints;
	private Integer taskMax;
	private String taskFreq;
	private int photoRequired;
	private int verificationRequired;
	
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getTaskAction() {
		return taskAction;
	}
	public void setTaskAction(String taskAction) {
		this.taskAction = taskAction;
	}
	public Integer getTaskPoints() {
		return taskPoints;
	}
	public void setTaskPoints(Integer taskPoints) {
		this.taskPoints = taskPoints;
	}
	public Integer getTaskMax() {
		return taskMax;
	}
	public void setTaskMax(Integer taskMax) {
		this.taskMax = taskMax;
	}
	public String getTaskFreq() {
		return taskFreq;
	}
	public void setTaskFreq(String taskFreq) {
		this.taskFreq = taskFreq;
	}
	public int getPhotoRequired() {
		return photoRequired;
	}
	public void setPhotoRequired(int photoRequired) {
		this.photoRequired = photoRequired;
	}
	public int getVerificationRequired() {
		return verificationRequired;
	}
	public void setVerificationRequired(int verificationRequired) {
		this.verificationRequired = verificationRequired;
	}
}
