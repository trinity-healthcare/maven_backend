/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */

package com.twp.domain;

// Class:	TaskDomainRowMapper
// Purpose:	Used to map values selected from the table task

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TaskDomainRowMapper implements RowMapper<TaskDomain>{

	@Override
	public TaskDomain mapRow(ResultSet rs, int rowNum) throws SQLException {
		TaskDomain task = new TaskDomain();
		
		task.setTaskId(rs.getInt("task_id"));
		task.setTaskName(rs.getString("task_name"));
		task.setTaskAction(rs.getString("task_action"));
		task.setTaskPoints(rs.getInt("task_points"));
		task.setTaskMax(rs.getInt("task_max"));
		task.setTaskFreq(rs.getString("task_freq"));
		task.setPhotoRequired(rs.getInt("photo_required"));
		task.setVerificationRequired(rs.getInt("verify_required"));
		
		return task;
	}

	
}
