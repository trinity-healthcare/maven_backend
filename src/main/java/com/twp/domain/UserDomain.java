/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */

package com.twp.domain;

// Functional fields of a user u
// Used to record a user's current point totals and goals
public class UserDomain {

	private String name;
	private int quarterGoal;
	private int quarterTotal;
	private int weekGoal;
	private int weekTotal;
	private String category;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuarterGoal() {
		return quarterGoal;
	}
	public void setQuarterGoal(int quarterGoal) {
		this.quarterGoal = quarterGoal;
	}
	public int getQuarterTotal() {
		return quarterTotal;
	}
	public void setQuarterTotal(int quarterTotal) {
		this.quarterTotal = quarterTotal;
	}
	public int getWeekGoal() {
		return weekGoal;
	}
	public void setWeekGoal(int weekGoal) {
		this.weekGoal = weekGoal;
	}
	public int getWeekTotal() {
		return weekTotal;
	}
	public void setWeekTotal(int weekTotal) {
		this.weekTotal = weekTotal;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
}
