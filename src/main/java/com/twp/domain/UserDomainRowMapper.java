/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */

package com.twp.domain;

// Class:	UserDomainRowMapper
// Purpose:	Used to map values selected from the table users

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UserDomainRowMapper implements RowMapper<UserDomain>{

	@Override
	public UserDomain mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserDomain user = new UserDomain();
		
		user.setName(rs.getString("name"));
		user.setQuarterGoal(rs.getInt("quarter_goal"));
		user.setQuarterTotal(rs.getInt("quarter_total"));
		user.setWeekGoal(rs.getInt("week_goal"));
		user.setWeekTotal(rs.getInt("week_total"));
		user.setCategory(rs.getString("category_id"));
		return user;
	}
}
