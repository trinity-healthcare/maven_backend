package com.twp.domain;

// Information provided by an admin to create a new user
public class UserInfoDomain {
	
	private int userInfoId;
	private String employeeName;
	private String employeeCode;
	private String employeePayrollCode;
	private String DOB;
	
	public int getUserInfoId() {
		return userInfoId;
	}
	public void setUserInfoId(int userInfoId) {
		this.userInfoId = userInfoId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String getEmployeePayrollCode() {
		return employeePayrollCode;
	}
	public void setEmployeePayrollCode(String employeePayrollCode) {
		this.employeePayrollCode = employeePayrollCode;
	}
	public String getDOB() {
		return DOB;
	}
	public void setDOB(String dOB) {
		DOB = dOB;
	}
}
