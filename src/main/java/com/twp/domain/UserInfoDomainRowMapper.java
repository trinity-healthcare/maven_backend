/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */

package com.twp.domain;

// Class:	UserInfoDomainRowMapper
// Purpose:	Used to map values selected from the table imported_users

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UserInfoDomainRowMapper implements RowMapper<UserInfoDomain>{

	@Override
	public UserInfoDomain mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserInfoDomain userInfo = new UserInfoDomain();
		
		userInfo.setEmployeeName(rs.getString("employee_name"));
		userInfo.setEmployeeCode(rs.getString("employee_code"));
		userInfo.setEmployeePayrollCode(rs.getString("employee_payroll_Code"));
		userInfo.setDOB(rs.getString("dob"));
		
		return userInfo;
	}

}
