/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.message.request;

//This class creates a form which is used for handling submitted_task JSON data from Angular.
public class EditUserTaskForm {
	
	private int userTaskId;
	private int taskId;
	private String userId;
	private String taskName;
	private int taskPoints;
	private String completionDate;
	private String photo;
	private String status;
	private String description;
	
	public int getUserTaskId() {
		return userTaskId;
	}
	public void setUserTaskId(int userTaskId) {
		this.userTaskId = userTaskId;
	}
	public int getTaskId() {
		return taskId;
	}
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public int getTaskPoints() {
		return taskPoints;
	}
	public void setTaskPoints(int taskPoints) {
		this.taskPoints = taskPoints;
	}
	public String getCompletionDate() {
		return completionDate;
	}
	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
