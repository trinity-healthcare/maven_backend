/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.message.request;

//This class creates a form which is used for handling new task JSON data from Angular.
public class NewAdminTaskForm {

	private int taskId;
	private String taskName;
	private String taskAction;
	private int taskPoints;
	private int taskMax;
	private String taskFreq;
	private boolean photoRequired;
	private boolean verificationRequired;
	
	public int getTaskId() {
		return taskId;
	}
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getTaskAction() {
		return taskAction;
	}
	public void setTaskAction(String taskAction) {
		this.taskAction = taskAction;
	}
	public int getTaskPoints() {
		return taskPoints;
	}
	public void setTaskPoints(int taskPoints) {
		this.taskPoints = taskPoints;
	}
	public int getTaskMax() {
		return taskMax;
	}
	public void setTaskMax(int taskMax) {
		this.taskMax = taskMax;
	}
	public String getTaskFreq() {
		return taskFreq;
	}
	public void setTaskFreq(String taskFreq) {
		this.taskFreq = taskFreq;
	}
	public boolean isPhotoRequired() {
		return photoRequired;
	}
	public void setPhotoRequired(boolean photoRequired) {
		this.photoRequired = photoRequired;
	}
	public boolean isVerificationRequired() {
		return verificationRequired;
	}
	public void setVerificationRequired(boolean verificationRequired) {
		this.verificationRequired = verificationRequired;
	}
}
