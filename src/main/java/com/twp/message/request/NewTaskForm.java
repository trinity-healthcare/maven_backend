/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.message.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

//This class creates a form which is used for handling submitted_task JSON data from Angular.
public class NewTaskForm {
	@NotBlank
    @Size(max = 100)
    private String task_name;
	
	@NotBlank
    @Size(max = 3)
    private String photo_required;
	
	@NotNull
    private int point_value;
	
	public String getTaskName() {
    	return task_name;
    }
    
    public void setTaskName(String task_name) {
    	this.task_name= task_name;
    }
    
    public String getPhotoRequired() {
    	return photo_required;
    }
    
    public void setPhotoRequired(String photo_required) {
    	this.photo_required = photo_required;
    }
    
    public int getPointValue() {
    	return point_value;
    }
    
    public void setPointValue(int point_value) {
    	this.point_value = point_value;
    }
}
