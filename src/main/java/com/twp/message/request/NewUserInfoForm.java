/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.message.request;

//This class creates a form which is used for handling imported_user JSON data from Angular.
public class NewUserInfoForm {
	
	private String name;
	private String employeeCode;
	private String payrollCode;
	private String DOB;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String getPayrollCode() {
		return payrollCode;
	}
	public void setPayrollCode(String payrollCode) {
		this.payrollCode = payrollCode;
	}
	public String getDOB() {
		return DOB;
	}
	public void setDOB(String dOB) {
		DOB = dOB;
	}	
}
