/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.message.request;

import java.util.Set;
import javax.validation.constraints.*;

//This class creates a form which is used for receiving JSON data for registering from Angular.
public class SignUpForm {
    @NotBlank
    @Size(min = 3, max = 50)
    private String name;

    @NotBlank
    @Size(min = 3, max = 50)
    private String username;

    @NotBlank
    @Size(max = 60)
    @Email
    private String email;
    
    private Set<String> role;
    
    @NotBlank
    @Size(min = 15, max = 40)
    private String password;
    
    private Long week_goal;
    
	private Long quarter_goal;
    
    private Long week_total;
    
    private Long quarter_total;
    
    @NotBlank
    @Size(max=250)
    private String question1;
    
    @NotBlank
    @Size(max=250)
    private String question2;
    
    @NotBlank
    @Size(max=250)
    private String question3;
    
    @NotBlank
    @Size(max=150)
    private String answer1;
    
    @NotBlank
    @Size(max=150)
    private String answer2;
    
    @NotBlank
    @Size(max=150)
    private String answer3;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Set<String> getRole() {
    	return this.role;
    }
    
    public void setRole(Set<String> role) {
    	this.role = role;
    }

	public Long getWeek_goal() {
		return week_goal;
	}

	public void setWeek_goal(Long week_goal) {
		this.week_goal = week_goal;
	}

	public Long getQuarter_goal() {
		return quarter_goal;
	}

	public void setQuarter_goal(Long quarter_goal) {
		this.quarter_goal = quarter_goal;
	}

	public Long getWeek_total() {
		return week_total;
	}

	public void setWeek_total(Long week_total) {
		this.week_total = week_total;
	}

	public Long getQuarter_total() {
		return quarter_total;
	}

	public void setQuarter_total(Long quarter_total) {
		this.quarter_total = quarter_total;
	}

	public String getQuestion1() {
		return question1;
	}

	public void setQuestion1(String question1) {
		this.question1 = question1;
	}

	public String getQuestion2() {
		return question2;
	}

	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	public String getQuestion3() {
		return question3;
	}

	public void setQuestion3(String question3) {
		this.question3 = question3;
	}

	public String getAnswer1() {
		return answer1;
	}

	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	public String getAnswer2() {
		return answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	public String getAnswer3() {
		return answer3;
	}

	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}
    
}