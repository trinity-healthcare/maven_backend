/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.message.request;

//This class creates a form which is used for handling user info JSON data
public class UserForm {
	private String name;
	private String username;
	private String category;
	private String weeklyTotal;
	private String quartlyTotal;
}
