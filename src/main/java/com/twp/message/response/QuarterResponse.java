/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.message.response;

//This class serves as a format for quarter point totals sent to the frontend
public class QuarterResponse {
	private String quarter1;
	private String quarter2;
	private String quarter3;
	private String quarter4;
	
	
	public QuarterResponse() {}
	
	public QuarterResponse(String quarter1, String quarter2, String quarter3,String quarter4) {
		this.quarter1 = quarter1;
		this.quarter2 = quarter2;
		this.quarter3 = quarter3;
		this.quarter4 = quarter4;
	}
}
