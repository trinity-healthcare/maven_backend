/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.message.response;

//This class serves as the format for question data being sent to the frontend
public class QuestionsResponse {
	private String question1;
	private String question2;
	private String question3;
	
	public QuestionsResponse() {};
	public QuestionsResponse(String question1, String question2, String question3) {
		this.question1 = question1;
		this.question2 = question2;
		this.question3 = question3;
	}
	public String getQuestion1() {
		return question1;
	}
	public void setQuestion1(String question1) {
		this.question1 = question1;
	}
	public String getQuestion2() {
		return question2;
	}
	public void setQuestion2(String question2) {
		this.question2 = question2;
	}
	public String getQuestion3() {
		return question3;
	}
	public void setQuestion3(String question3) {
		this.question3 = question3;
	}
	
}
