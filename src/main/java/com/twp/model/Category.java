/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */
package com.twp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

// Health categories to which users are assigned to determine their point goals
@Entity
@Table(name = "category")
public class Category {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long category_id;

    @NotBlank
    @Size(min=3, max = 50)
    private String name;
    
    private Long quarterly_goal;
    
    @Size(max=255)
    private String description;

	public Long getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Long category_id) {
		this.category_id = category_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getQuarterly_goal() {
		return quarterly_goal;
	}

	public void setQuarterly_goal(Long quarterly_goal) {
		this.quarterly_goal = quarterly_goal;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
    
    public Category() {}
    
    public Category(String name, Long quarterly_goal, String description) {
    	this.name = name;
    	this.quarterly_goal = quarterly_goal;
    	this.description = description;
    }
    
}
