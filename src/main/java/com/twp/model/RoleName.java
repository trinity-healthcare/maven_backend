/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */
package com.twp.model;

// All possible roles a user can have
// Admins have total control over viewing and editing user data and approve user's logged tasks
// Moderators can view users data to check for compliance
// Users log tasks (created by admins) to earn points
public enum  RoleName {
	ROLE_ADMIN, ROLE_MODERATOR, ROLE_USER
    
}
