/* 
 *  Copyright (C) 2019 Prime Inc - All Rights Reserved
 *  Unauthorized use of this file and its contents, via any medium is strictly prohibited.
 *  Authored by the Missouri State University Computer Science Department
 *  Fall 2019 CSC 450 - Group 2
 */
package com.twp.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

import com.twp.model.Role;
// Direct mapping to User table in db

@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
            "username"
        }),
        @UniqueConstraint(columnNames = {
            "email"
        })
})
public class User{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(min=3, max = 50)
    private String name;

    @NotBlank
    @Size(min=3, max = 50)
    private String username;

    @NaturalId
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(min=6, max = 100)
    private String password;
    
    private Long week_goal;
    
	private Long quarter_goal;
    
    private Long week_total;
    
    private Long quarter_total;
    
    @Size(max=250)
    private String question1;
    
    @Size(max=250)
    private String question2;
    
    @Size(max=250)
    private String question3;
    
    @Size(max=150)
    private String answer1;
    
    @Size(max=150)
    private String answer2;
    
    @Size(max=150)
    private String answer3;
    
    private String payroll_code;
    
    private String smoker;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles", 
    	joinColumns = @JoinColumn(name = "user_id"), 
    	inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();
    
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
    
    public User() {}

    public User(String name, String username, String email, String password, Long week_goal, Long quarter_goal, Long week_total, Long quarter_total,
    		String question1, String question2, String question3, String answer1, String answer2, String answer3, String payroll_code) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.week_goal = week_goal;
        this.quarter_goal = quarter_goal;
        this.week_total = week_total;
        this.quarter_total = quarter_total;
        this.question1 = question1;
        this.question2 = question2;
        this.question3 = question3;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.payroll_code = payroll_code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
    
    public Long getWeek_goal() {
		return week_goal;
	}

	public void setWeek_goal(Long week_goal) {
		this.week_goal = week_goal;
	}

	public Long getQuarter_goal() {
		return quarter_goal;
	}

	public void setQuarter_goal(Long quarter_goal) {
		this.quarter_goal = quarter_goal;
	}

	public Long getWeek_total() {
		return week_total;
	}

	public void setWeek_total(Long week_total) {
		this.week_total = week_total;
	}

	public Long getQuarter_total() {
		return quarter_total;
	}

	public void setQuarter_total(Long quarter_total) {
		this.quarter_total = quarter_total;
	}

	public String getQuestion1() {
		return question1;
	}

	public void setQuestion1(String question1) {
		this.question1 = question1;
	}

	public String getQuestion2() {
		return question2;
	}

	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	public String getQuestion3() {
		return question3;
	}

	public void setQuestion3(String question3) {
		this.question3 = question3;
	}

	public String getAnswer1() {
		return answer1;
	}

	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	public String getAnswer2() {
		return answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	public String getAnswer3() {
		return answer3;
	}

	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}

	

	public String getPayroll_code() {
		return payroll_code;
	}

	public void setPayroll_code(String payroll_code) {
		this.payroll_code = payroll_code;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getSmoker() {
		return smoker;
	}

	public void setSmoker(String smoker) {
		this.smoker = smoker;
	}	
}