/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.twp.model.Question;

//This interface extends JpaRepository for QuestionRepository to create a QuestionRepository
@Repository
public interface QuestionRepository  extends JpaRepository<Question, Long>{

}
