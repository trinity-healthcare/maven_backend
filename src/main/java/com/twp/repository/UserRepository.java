/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.twp.model.User;

//This interface extends JpaRepository for User to create a UserRepository
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
    Optional<User> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
    
    @Query(value="SELECT u from User u where u.quarter_total >= u.quarter_goal")
    List<User> findCompliantUsers();
    
    @Query(value="SELECT u FROM User u WHERE u.category.name = Default")
    List<User> findUnclassifiedUsers();
    
    @Query(value="SELECT u from User u where u.quarter_total < u.quarter_goal")
    List<User> findNonCompliantUsers();
}