/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.security.services;

import com.twp.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.twp.security.services.UserPrinciple;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.constraints.Size;

//This class builds a user which is used by the security classes
public class UserPrinciple implements UserDetails {
	private static final long serialVersionUID = 1L;

	private Long id;

    private String name;

    private String username;

    private String email;
    
    private Long week_goal;

	private Long quarter_goal;
    
    private Long week_total;
    
    private Long quarter_total;
    
    @Size(max=250)
    private String question1;
    
    @Size(max=250)
    private String question2;
    
    @Size(max=250)
    private String question3;
    
    @Size(max=150)
    private String answer1;
    
    @Size(max=150)
    private String answer2;
    
    @Size(max=150)
    private String answer3;
    
    private String payroll_code;
    
    private String smoker;

    @JsonIgnore
    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    public UserPrinciple(Long id, String name, 
			    		String username, String email, String password, Long week_goal, Long quarter_goal, Long week_total, Long quarter_total,
			    		String question1, String question2, String question3, String answer1, String answer2, String answer3, String payroll_code,
			    		String smoker, Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.week_goal = week_goal;
        this.quarter_goal = quarter_goal;
        this.week_total = week_total;
        this.quarter_total = quarter_total;
        this.question1 = question1;
        this.question2 = question2;
        this.question3 = question3;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.payroll_code = payroll_code;
        this.smoker = smoker;
        this.authorities = authorities;
    }

    public static UserPrinciple build(User user) {
        List<GrantedAuthority> authorities = user.getRoles().stream().map(role ->
                new SimpleGrantedAuthority(role.getName().name())
        ).collect(Collectors.toList());

        return new UserPrinciple(
                user.getId(),
                user.getName(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getWeek_goal(),
                user.getQuarter_goal(),
                user.getWeek_total(),
                user.getQuarter_total(),
                user.getQuestion1(),
                user.getQuestion2(),
                user.getQuestion3(),
                user.getAnswer1(),
                user.getAnswer2(),
                user.getAnswer3(),
                user.getPayroll_code(),
                user.getSmoker(),
                authorities
        );
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }
    
    public Long getWeek_goal() {
		return week_goal;
	}

	public Long getQuarter_goal() {
		return quarter_goal;
	}

	public Long getWeek_total() {
		return week_total;
	}

	public Long getQuarter_total() {
		return quarter_total;
	}
	
    public String getPayroll_code() {
		return payroll_code;
	}

	public String getSmoker() {
		return smoker;
	}

	@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        UserPrinciple user = (UserPrinciple) o;
        return Objects.equals(id, user.id);
    }
}