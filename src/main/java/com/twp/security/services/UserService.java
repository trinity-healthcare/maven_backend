/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.security.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.twp.model.User;

//This service allows for user information to be retrieved
@Service
@Transactional
public interface UserService {

	Optional<User> getUserByUsername(String username);

	List<User> findAllUsers();

	void updateUser(User user);	

}
