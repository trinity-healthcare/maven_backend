/* 
 * Copyright (C) 2019 Prime Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * All source code in this project is proprietary and confidential.
 * Authored by the Missouri State University Computer Science Department
 * Fall 2019 CSC 450 - Group 2
 */

package com.twp.security.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.twp.model.Task;
import com.twp.model.User;
import com.twp.repository.UserRepository;


//This class is an implementation of the UserService class
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;
	
	
	@Override
    public Optional<User> getUserByUsername(String username){
        return userRepository.findByUsername(username);
    }
	
	@Override
    public List<User> findAllUsers(){
        return userRepository.findAll();
    }
	@Override
	public void updateUser(User user) {
		userRepository.save(user);
	}
}
